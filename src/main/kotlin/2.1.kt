/*
* AUTHOR: Marçal Herraiz Prat
* DATE: 2022/12/15
* TITLE: Factorial
*/

fun main() {
    val numero = readln().toInt()

    println(factorial(numero))
}

fun factorial(numero: Int): Int {
    return if (numero==1) numero
    else numero * factorial(numero-1)
}
