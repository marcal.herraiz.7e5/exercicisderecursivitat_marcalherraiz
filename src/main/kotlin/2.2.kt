/*
* AUTHOR: Marçal Herraiz Prat
* DATE: 2022/12/15
* TITLE: Doble factorial
*/

fun main() {

    val numero = readln().toInt()

    println(dobleFactorial(numero))

}

fun dobleFactorial(numero: Int): Int {
    return if (numero<1) 1
    else numero * dobleFactorial(numero-2)
}

//Recordeu que n!! = n × (n − 2) × (n − 4)
//× ...
//Exemple: 9!! = 9 x 7 x 5 x 3 x 1