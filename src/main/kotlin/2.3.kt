/*
* AUTHOR: Marçal Herraiz Prat
* DATE: 2022/12/15
* TITLE: Nombre de dígits
*/

fun main() {
    val numero = readln().toInt()

    println(nombreDigits(numero))
}

fun nombreDigits(num :Int): Int{
    return if ( num<=9){
        1
    } else
        1 + nombreDigits( num/10 )
}


