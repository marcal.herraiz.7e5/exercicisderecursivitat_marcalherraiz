/*
* AUTHOR: Marçal Herraiz Prat
* DATE: 2022/12/15
* TITLE: Nombres creixents
*/

fun main() {
    val numero = readln().toInt()

    println(nombreCreixent(numero))
}

// 1 2 3 4 5

fun nombreCreixent(numero: Int): Boolean {

    val num = numero.toString()
    val primerDigit = num[0].toString().toInt()

    val seguentDigit: Int = if (numero <= 9) {
        numero
    } else {
        num[1].toString().toInt()
    }

    return if ((primerDigit == seguentDigit || primerDigit < seguentDigit)&& numero>9 ) {
        nombreCreixent(num.substring(1).toInt())
    } else {
        numero < 10
    }
}



