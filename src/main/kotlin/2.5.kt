/*
* AUTHOR: Marçal Herraiz Prat
* DATE: 2022/12/15
* TITLE: Reducció de dígits
 */

fun main() {
    val numero = readln().toInt()

    println(reduccioDigit(numero))
}

fun reduccioDigit(numero: Int): Int {
    var suma = 0
    for (digit in numero.toString()) suma+=digit.toString().toInt()
    return if (suma<=9){
        suma
    }else{
        reduccioDigit(suma)
    }
}
