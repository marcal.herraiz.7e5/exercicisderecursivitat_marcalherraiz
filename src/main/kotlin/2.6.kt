/*
* AUTHOR: Marçal Herraiz Prat
* DATE: 2022/12/16
* TITLE: Seqüència d’asteriscos
 */

fun main() {
  asterisksSequence(readln().toInt())
}

fun asterisksSequence(numero: Int) {
   var sequence = ""
    repeat(numero){sequence+= "*"}

    if (numero <= 1) println(sequence)
    else{
        asterisksSequence(numero-1)
        println(sequence)
        asterisksSequence(numero-1)
    }
}

