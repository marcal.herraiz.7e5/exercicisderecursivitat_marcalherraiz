/*
* AUTHOR: Marçal Herraiz Prat
* DATE: 2022/12/16
* TITLE: Primers perfectes
 */

fun main() {
    println(primerPerfecte(readln().toInt()))
}

fun primerPerfecte(numero: Int) : Boolean{
    var suma = 0
    for (digit in numero.toString()) {
        suma+= digit.toString().toInt()
    }
    val isPrime = checkPrime(suma.toString().toInt())

    return if (suma>9 && !isPrime) primerPerfecte(suma)
    else isPrime
}

fun checkPrime(numero: Int): Boolean {
    var i = 2
    while (i < numero && numero % i != 0) i++
    return i == numero

}

