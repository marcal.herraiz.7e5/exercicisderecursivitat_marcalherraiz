/*
* AUTHOR: Marçal Herraiz Prat
* DATE: 2022/12/16
* TITLE: Torres de Hanoi
 */

fun main() {

    torresHanoi(readln().toInt(), "A", "B", "C")
}

fun torresHanoi(numeroDiscos: Int, A: String, B: String, C: String) {
    if (numeroDiscos == 1){
        println("$A => $C")
    }else{
        torresHanoi(numeroDiscos-1, A, C, B)
        println("$A => $C")
        torresHanoi(numeroDiscos-1, B, A, C)
    }
}

